const express = require("express");
const router = express.Router();

const courseController = require("../Controllers/courseController.js");
const auth = require("../auth.js");

//S39-D1
	//Route for creating a course
	// router.post("/", courseController.addCourse);

//S39-A1
//Route for creating a course (for admin only)
	router.post("/", auth.verify, courseController.addCourse);

//S40-D1
 // Route for retreiving all course
	router.get("/all", auth.verify, courseController.allCourses);

 //Route for retrieving all active courses
	router.get("/allActive", courseController.allActiveCourses);

 //Route for retrieving all inactive courses
	router.get("/allInactive", auth.verify, courseController.allInactiveCourses);


 // [ Route with Params]

 //Route for retrieving details of specific course
	router.get("/:courseId", courseController.courseDetails);

 // Route for updating Course by ID
router.put("/update/:courseId", auth.verify, courseController.updateCourse)


//S40-A1
// Route for Archiving COurse by ID
router.put("/archive/:courseId", auth.verify, courseController.archiveCourse)

module.exports = router;