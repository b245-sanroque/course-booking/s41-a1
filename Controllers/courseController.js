const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");

// Create a new course
 /*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database

	// create
		{
		    "name": "HTML",
		    "description": "Learn the basics of Web Development",
		    "price": 1000
		}
 */

// AddCourse Controller (for admin only)
 module.exports.addCourse = (request, response) => {
	
	//S39-A1
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(userData.isAdmin === true){
		let input = request.body;

		let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});

		// saves the created object to our database
		return newCourse.save()
		// course successfully created
		.then(course => {
			console.log(course);
			response.send(`Course Added!`);
		})
		// course creation failed
		.catch(error => {
			console.log(error);
			response.send(false);
		})
	} else{
		return response.send(`You are not authorized to do this action!`)
	}
 }

// S40-D1
// Retrieve allCourses - Active & Inactive (admin only)
 module.exports.allCourses = (request, response) => {
 	const userData = auth.decode(request.headers.authorization);

 	if(!userData.isAdmin){
 		return response.send(`You don't have access to this route`)
 	} else{
 		Course.find({})
 		.then(result => response.send(result))
 		.catch(error => response.send(error));
 	}
 }

// Retrieve allActiveCourse (admin only)
 module.exports.allActiveCourses = (request, response) => {
 	Course.find({isActive: true})
 	.then(result => response.send(result))
 	.catch(error => response.send(error));
 }

// Mini-Activity
	// 1. You are going to create a route wherein it can retrieve all inactive courses.
	// 2. Make sure that the admin users only are the ones that can acces this route.
// Retrieve allInactiveCourses (admin only)
 module.exports.allInactiveCourses = (request, response) => {
 	const userData = auth.decode(request.headers.authorization);

 	if(!userData.isAdmin){
 		return response.send(`You don't have access to this route`)
 	} else{
 		Course.find({isActive: false})
 		.then(result => response.send(result))
 		.catch(error => response.send(error));
 	}
 }

// This controller will get the details of specific course
 module.exports.courseDetails = (request, response) => {
 	//to get the params from the url
 	const courseId = request.params.courseId;

 	Course.findById(courseId)
 	.then(result => response.send(result))
 	.catch(error => response.send(error));
 }


// This controller is for updating specific course
 // Business logic
	 // 1. We are going to update the course that is stored in the params
  module.exports.updateCourse = (request, response) => {

  	const userData = auth.decode(request.headers.authorization);
  	const courseId = request.params.courseId;
  	const input = request.body;

  	if(!userData.isAdmin){
  		return response.send(`You don't have access in this page!`)
  	} else{
  		Course.findById(courseId)
  		.then(result => {
  			if(result === null){
  				return response.send("CourseID provided is invalid!")
  			} else{
  					let updatedCourse = {
  						name: input.name,
  						description: input.description,
  						price: input.price
  					}
  					Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
  					.then(result => {
  						response.send(result)
  					})
  					.catch(error => response.send(error))
  				}
  		}).catch(error => response.send(error));
  	}
 }

//S40-A1
// This controller is for archiving specific course
 module.exports.archiveCourse = async (request, response) => {
  	const userData = auth.decode(request.headers.authorization);
  	const courseId = request.params.courseId;
  	const input = request.body;

  	if(!userData.isAdmin){
  		return response.send(`You don't have access in this page!`)
  	} else{
  		await Course.findById(courseId)
  		.then(result => {
  			if(result === null){
  				return response.send("CourseID provided is invalid!")
  			} else{
  					let archivedCourse = {
  						isActive: input.isActive
  					}
  					Course.findByIdAndUpdate(courseId, archivedCourse, {new: true})
  					.then(result => {
  						response.send(`Course successfully archived!`)
  					})
  					.catch(error => response.send(error))
  				 }
  		}).catch(error => response.send(error));
	 }
 }